<?php 

function kapital($str) {
    $output = '';
    for ($i=0; $i < strlen($str); $i++) { 
        if(ctype_lower($str[$i])) {
            $output .= strtoupper($str[$i]);
        } else {
            $output .= strtolower($str[$i]);
        }
    }
    return $output . "<br/>";
}

echo kapital("Hallo");
